import { Component } from '@angular/core';
import { faHeart, faBars } from '@fortawesome/free-solid-svg-icons';
import { SidenavService } from './../sidenav';

@Component({
    selector: 'header-component',
    templateUrl: './../../../../theme/partials/header/index.pug'
})

export class HeaderComponent {

	faBars = faBars;
	faHeart = faHeart;

	constructor(private sidenavService: SidenavService) {}

	openPanel() {
		this.sidenavService.open();
	}
}
