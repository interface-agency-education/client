import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgImageSliderModule } from 'ng-image-slider';
import { ProjectListComponent } from './list/component';
import { ProjectViewComponent } from './view/component';
import { ProjectListState } from './list/state';
import { CategoryFilterPipe } from './list/category';
import { ComposeBase64ImagePipe } from './list/pipe';
import { SubCategoryFilterPipe } from './list/sub.category';
import { TextFilterPipe } from './list/filter';
import { ProjectRoutingModule } from './routes';
import { SharedModule } from './../shared/module';

@NgModule({
    declarations: [
		CategoryFilterPipe,
		ComposeBase64ImagePipe,
		SubCategoryFilterPipe,
		TextFilterPipe,
        ProjectListComponent,
        ProjectViewComponent
    ],
    entryComponents: [],
    imports: [
		CommonModule,
		FormsModule,
        ReactiveFormsModule,
		NgImageSliderModule,
		ProjectRoutingModule,
		SharedModule
    ],
    exports: [],
    providers: [
		ProjectListState
	]
})

export class ProjectModule {}
