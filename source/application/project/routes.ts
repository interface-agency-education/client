import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProjectListComponent } from './list/component';
import { ProjectListResolve } from './list/resolve';
import { ProjectViewComponent } from './view/component';
import { ProjectViewResolve } from './view/resolve';

const routes: Routes = [{
	path: '',
	component: ProjectListComponent,
	resolve: {
		projects: ProjectListResolve
	}
}, {
	path: ':project_clean',
	component: ProjectViewComponent,
	resolve: {
		project: ProjectViewResolve
	}
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectRoutingModule {}
