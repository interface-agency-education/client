export interface ProjectInterface {
    _id: string,
	category: string,
	clean: string,
	creator: {
		name: string,
		title: string
	},
	content: string[],
	description: string,
	images: [{
		base64: string,
		file_type: string
	}],
	medium: string,
	quote: string,
	testimonial: {
		name: string,
		headshot: {
			base64: string,
			file_type: string
		},
		statement: string
	},
	thumb: {
		base64: string,
		file_type: string
	},
	title: string
};
