import { Pipe, PipeTransform } from '@angular/core';
import { ProjectInterface } from './../interface';

@Pipe({
    name: 'textFilter',
	pure: true
})

export class TextFilterPipe implements PipeTransform {

    transform(list: ProjectInterface[], filter?:string): ProjectInterface[] {
        if (!filter) return list;
        return list.filter(project => project.title.includes(filter));
    }
}
