export interface ArticleSearchInterface {
	category: string,
	categories: string[],
	sub_category: string,
	sub_categories: string[]
};
