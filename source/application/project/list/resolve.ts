import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { ProjectInterface } from './../interface';
import { ProjectListService } from './service';

@Injectable({
    providedIn: 'root'
})

export class ProjectListResolve implements Resolve<ProjectInterface[]> {

    constructor(private ProjectListService: ProjectListService) {}

    resolve() {
        return this.ProjectListService.get()
			.then((response: ProjectInterface[]) => {
				return response;
			});
	}
}
