import { Pipe, PipeTransform } from '@angular/core';
import { ProjectInterface } from './../interface';
@Pipe({
    name: 'categoryFilter',
	pure: true
})

export class CategoryFilterPipe implements PipeTransform {

    transform(list: ProjectInterface[], filter?:string): ProjectInterface[] {
        if (!filter) return list;
        return list.filter(project => project.category === filter);
    }
}
