import { Pipe, PipeTransform } from '@angular/core';
import { ProjectInterface } from './../interface';
@Pipe({
    name: 'subCategoryFilter',
	pure: true
})

export class SubCategoryFilterPipe implements PipeTransform {

    transform(list: ProjectInterface[], filter?:string): ProjectInterface[] {
        if (!filter) return list;
        return list.filter(project => project.medium === filter);
    }
}
