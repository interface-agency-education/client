import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProjectInterface } from './../interface';

@Injectable({
    providedIn: 'root'
})

export class ProjectListService {

    constructor(private httpClient: HttpClient) {}

    get(): Promise<ProjectInterface[]> {
		// return this.httpClient.get<ProjectInterface[]>('http://localhost:8080')
		return this.httpClient.get<ProjectInterface[]>('https://us-central1-interface-agency.cloudfunctions.net/api-student-project-list')
			.toPromise()
			.then(function(response) {
				return response;
			});
    }
}
