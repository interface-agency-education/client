import { Injectable } from '@angular/core';
import { ProjectInterface } from './../interface'
import { ArticleSearchInterface } from './interface/search'

@Injectable()

export class ProjectListState {

    private state:ArticleSearchInterface = {
		category: '',
		categories: [],
		sub_category: '',
		sub_categories: []
	};

	constructor() {}

	buildCategoriesList(projects:ProjectInterface[]) {
		let categories:string[] = [];
		for(let i=0; i<projects.length; i++) {
			categories.push(projects[i].category);
		}
		this.state.categories = categories.filter((item, index) => {
			if(categories.indexOf(item) == index) return item;
		});
		if(this.state.category !== '') this.buildSubCategoriesList(projects);
	}

	buildSubCategoriesList(projects:ProjectInterface[]) {
		let sub_categories:string[] = [];
		for(let i=0; i<projects.length; i++) {
			if(projects[i].category === this.state.category) {
				sub_categories.push(projects[i].medium);
			}
		}
		this.state.sub_categories = sub_categories.filter((item, index) => {
			if(sub_categories.indexOf(item) == index) return item;
		});
	}

    load(projects:ProjectInterface[]) {
		if(this.state.category === '') this.state.category = localStorage.getItem('interface_design_category') || '';
		if(this.state.sub_category === '') this.state.sub_category = localStorage.getItem('interface_design_sub_category') || '';
		this.buildCategoriesList(projects);
        return this.state;
    }

	setCategory(value:string, projects:ProjectInterface[]):ArticleSearchInterface {
		this.state.category = value;
		localStorage.setItem('interface_design_category', value);
		this.buildCategoriesList(projects);
		return this.state;
    }

	setSubCategory(value:string): void {
		this.state.sub_category = value;
		localStorage.setItem('interface_design_sub_category', this.state.sub_category)
    }

	reset():void {
		this.state.category = '';
		this.state.sub_category = '';
		localStorage.removeItem('interface_design_category');
		localStorage.removeItem('interface_design_sub_category');
	}
}
