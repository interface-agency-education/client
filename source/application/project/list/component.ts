import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ProjectInterface } from './../interface';
import { ArticleSearchInterface } from './interface/search';
import { ProjectListState } from './state';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'project-list',
	templateUrl: './../../../../../theme/partials/project/list/index.pug',
})

export class ProjectListComponent implements OnInit {

	faTimes = faTimes;

	public availableCategories:string[] = [];
	public availableSubCategories:string[] = [];
	public cachedArticles: ProjectInterface[] = [];
	public projects: ProjectInterface[] = [];
	public search:ArticleSearchInterface;

	searchForm: FormGroup;

	constructor(private activatedRoute: ActivatedRoute, public formBuilder: FormBuilder, private ProjectListState: ProjectListState, private router: Router) {
		this.searchForm = this.formBuilder.group({
			category: [''],
			filter: [''],
			sub_category: ['']
		});
	}

	ngOnInit() {
		this.projects = this.activatedRoute.snapshot.data.projects;
		// this.search = this.ProjectListState.load(this.projects);
		console.log('projects:', this.projects);
		// this.searchForm.controls['category'].setValue(this.search.category);
		// this.searchForm.controls['sub_category'].setValue(this.search.sub_category);
	}

	setCategory() {
		this.search = this.ProjectListState.setCategory(this.searchForm.value.category, this.projects);
		this.searchForm.controls['sub_category'].setValue('');
	}

	setSubCategory() {
		this.ProjectListState.setSubCategory(this.searchForm.value.sub_category);
	}

	viewProject(project_clean:string) {
		this.router.navigate([project_clean], { relativeTo: this.activatedRoute.parent });
	}

	resetSearchForm() {
		this.searchForm.controls['category'].setValue('');
		this.searchForm.controls['filter'].setValue('');
		this.searchForm.controls['sub_category'].setValue('');
		this.ProjectListState.reset();
	}
}
