import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProjectInterface } from './../interface';

@Injectable({
    providedIn: 'root'
})

export class ProjectFindService {

    constructor(private httpClient: HttpClient) {}

    get(project_clean:string): Promise<ProjectInterface> {
		// return this.httpClient.get<ProjectInterface>('http://localhost:8080?project_clean=' + project_clean)
		return this.httpClient.get<ProjectInterface>('https://us-central1-interface-agency.cloudfunctions.net/api-student-project-find?project_clean=' + project_clean)
			.toPromise()
			.then(function(response) {
				return response;
			});
    }
}
