import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { ProjectInterface } from './../interface';
import { ProjectFindService } from './service';

@Injectable({
    providedIn: 'root'
})

export class ProjectViewResolve implements Resolve<any> {

    constructor(private projectFindService: ProjectFindService) {}

    resolve(activatedRouteSnapshot: ActivatedRouteSnapshot) {
        return this.projectFindService.get(activatedRouteSnapshot.params.project_clean)
			.then((response: ProjectInterface) => {
				return response;
			});
	}
}
