import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { ProjectInterface } from './../interface';

@Component({
    selector: 'project-view',
	templateUrl: './../../../../../theme/partials/project/view/index.pug',
})

export class ProjectViewComponent implements OnInit {

	faTimes = faTimes;
	public imageObject:Array<object> = [];
	public project: ProjectInterface;

	constructor(private activatedRoute: ActivatedRoute, private domSanitizer: DomSanitizer) {}

	ngOnInit() {
		this.project = this.activatedRoute.snapshot.data.project[0];
		console.log('project:', this.project);
	}

	composeBase64Image(base64String:string, fileType:string) {
		return 'data:image/' + fileType + ';base64,' + (this.domSanitizer.bypassSecurityTrustResourceUrl(base64String) as any).changingThisBreaksApplicationSecurity;
	}

}
