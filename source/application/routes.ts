import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/component';
import { AboutComponent } from './about/component';
import { ContactComponent } from './contact/component';
import { ErrorComponent } from './error/component';

const routes: Routes = [{
	path: '',
	component: HomeComponent,
}, {
	path: 'about',
	component: AboutComponent,
}, {
	path: 'projects',
	loadChildren: () => import('./project/module').then(module => module.ProjectModule)
}, {
	path: 'contact',
	component: ContactComponent,
}, {
	path: 'page-not-found',
	component: ErrorComponent
}, {
	path: '**',
	redirectTo: 'page-not-found'
}];

@NgModule({
  imports: [ RouterModule.forRoot(routes)],
  exports: [ RouterModule ]
})

export class RootRoutingModule {}
