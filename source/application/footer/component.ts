import { Component } from '@angular/core';
import { faFacebookF, faTwitter, faInstagram } from '@fortawesome/free-brands-svg-icons';

@Component({
    selector: 'footer-component',
    templateUrl: './../../../../theme/partials/footer/index.pug'
})

export class FooterComponent {

	faFacebookF = faFacebookF;
	faTwitter = faTwitter;
	faInstagram = faInstagram;

}
