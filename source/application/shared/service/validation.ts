import { Injectable } from "@angular/core";

@Injectable()
export class ValidationMessageService {

    public getValidationMessage(validationId:string):string{
        return this.errorMessages[validationId];
    }

    private errorMessages = {
        'firstname-required-message' : "First name is required",
        'firstname-minlength-message' : "First name must have 8 characters",
        'firstname-maxlength-message' : "First name can have maximum 30 characters",

        'lastname-required-message' : "Last name is required",
        'lastname-minlength-message' : "Lastname must have 8 characters",
        'lastname-maxlength-message' : "Lastname can have maximum 30 characters",

        'email-required-message': 'Email is required',
        'email-email-message': 'Email is not in valid format',
        'email-emailDoesNotExist-message': 'Email does not exist',

		'password-required-message' : "Password is a required field",
		'password-minlength-message' : "Password must have a minimum of 8 characters",
		'password-maxlength-message' : "Password can have maximum 16 characters",
		'password-match-message' : "New password must be different than your existing password",
		'password-unmatching-message' : "Passwords do not match",
		'password-incorrect-message' : "Password is incorrect",
    }

}
