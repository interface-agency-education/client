import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormControlValidationDirective } from './directive/validation';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
    declarations: [
		FormControlValidationDirective,
    ],
    imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		FontAwesomeModule
    ],
	providers: [],
	exports: [
		FontAwesomeModule,
		FormControlValidationDirective
	]
})

export class SharedModule {}
