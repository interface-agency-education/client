import { Directive, Input, HostListener, ElementRef, OnInit, OnDestroy } from '@angular/core';
import { NgControl, ValidationErrors } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ValidationMessageService } from './../service/validation';

@Directive({
    selector: '[formControlValidation]'
})
export class FormControlValidationDirective implements OnInit, OnDestroy {

    constructor(private elRef: ElementRef, private control: NgControl, private validationMessageService: ValidationMessageService) {}

    @Input('validationMessageId') validationMessageId: string;
    @Input() ngModel: string;

    errorUniqueId: string = '';
    previousValue: string = '';
    statusChangeSubscription: Subscription;
    valueChangeSubscription: Subscription;

    ngOnInit():void {
        this.errorUniqueId = this.validationMessageId + '-' + new Date().getTime() + '-error-message';
        this.statusChangeSubscription = this.control.statusChanges.subscribe((status) => {
            if (status == 'INVALID' && !this.control.errors.minlength && !this.control.errors.maxlength) {
				this.showError();
			} else {
				this.removeError();
			}
        });

        this.valueChangeSubscription = this.control.valueChanges.subscribe((str:any) => {
			if (str !== this.previousValue) {
				this.removeError();
			}
			this.previousValue = str;
        });
    }

    ngOnDestroy():void {
        this.statusChangeSubscription.unsubscribe();
        this.valueChangeSubscription.unsubscribe();
    }

    @HostListener('blur', ["$event"]) handleBlurEvent() {
        if (this.control.errors) {
			this.showError();
		} else {
			this.removeError();
		}
    }

	@HostListener('input', ["$event"]) handleInputEvent() {
		this.removeError();
	}
    private showError():void {
        this.removeError();
        const validationErrors: ValidationErrors = this.control.errors;
        const firstKey = Object.keys(validationErrors)[0];
        const errorMessageKey = this.validationMessageId + '-' + firstKey + '-message';
        const errorMessage = this.validationMessageService.getValidationMessage(errorMessageKey);
        const errorTemplate = '<div class="i-validation" id="' + this.errorUniqueId + '"><div class="i-validation__content"><div class="i-validation__text"><h6>' + errorMessage + '</h6></div></div><div class="i-validation__content"><div class="i-validation__caret"><div class="i-validation__icon"></div></div></div></div>'
        this.elRef.nativeElement.parentElement.insertAdjacentHTML('beforeend', errorTemplate);
    }

    private removeError():void {
        const errorElement = document.getElementById(this.errorUniqueId);
        if (errorElement) errorElement.remove();
    }

}
