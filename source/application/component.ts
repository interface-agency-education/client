import { Component, ViewEncapsulation, ViewChild } from '@angular/core';
import { SidenavService } from './sidenav';
import { MatSidenav } from '@angular/material';
import { faCaretLeft } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'root',
  templateUrl: './../../../theme/partials/index.pug',
  encapsulation: ViewEncapsulation.None
})
export class RootComponent {

	@ViewChild('sidenav', {static: false}) public sidenav: MatSidenav;

	faCaretLeft = faCaretLeft;

	constructor(private sidenavService: SidenavService) {}

  	ngAfterViewInit(): void {
  		this.sidenavService.setSidenav(this.sidenav);
  	}

	closePanel() {
		this.sidenavService.close();
	}
}
