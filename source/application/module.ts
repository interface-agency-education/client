import { ApplicationRef, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { removeNgStyles, createNewHosts, createInputTransfer } from '@angularclass/hmr';
import { MatSidenavModule } from '@angular/material';

import { RootRoutingModule } from './routes';
import { ProjectModule } from './project/module';
import { SharedModule } from './shared/module';
import { RootComponent } from './component';
import { HeaderComponent } from './header/component';
import { FooterComponent } from './footer/component';
import { HomeComponent } from './home/component';
import { AboutComponent } from './about/component';
import { ContactComponent } from './contact/component';
import { ErrorComponent } from './error/component';
import { SidenavService } from './sidenav';

require('./../../../theme/styles/index.styl')

@NgModule({
    declarations: [
        RootComponent,
		HeaderComponent,
		FooterComponent,
        HomeComponent,
        AboutComponent,
        ContactComponent,
        ErrorComponent,
    ],
    entryComponents: [],
    imports: [
        RootRoutingModule,
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
		MatSidenavModule,
		ProjectModule,
		SharedModule
    ],
    bootstrap: [RootComponent],
    exports: [RootComponent],
    providers: [
		SidenavService,
	]
})

export class RootModule {

    constructor(public applicationRef: ApplicationRef) {}

    hmrOnInit(store) {
        if (!store || !store.state) return;
        // console.log('HMR store', store);
        // console.log('store.state.data:', store.state.data)
        if ('restoreInputValues' in store) {
            store.restoreInputValues();
        }
        this.applicationRef.tick();
        delete store.state;
        delete store.restoreInputValues;
    }
    hmrOnDestroy(store) {
        var cmpLocation = this.applicationRef.components.map(cmp => cmp.location.nativeElement);
        store.disposeOldHosts = createNewHosts(cmpLocation)
        store.restoreInputValues = createInputTransfer();
        removeNgStyles();
    }
    hmrAfterDestroy(store) {
        store.disposeOldHosts()
        delete store.disposeOldHosts;
    }
}
